import roomsDatabase from '../database/rooms';
import { texts } from '../database/data';
import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME,
  MINIMUM_USERS_FOR_GAME_START
} from '../config';
import { joinRoom, createNewRoom, leaveRoom, updateActiveRooms } from './roomsService';
import commentator from './commentatorService';

const timers = new Map();

const setGameTimer = (roomId, timer) => timers.set(roomId, timer);
const clearGameTimer = roomId => clearInterval(timers.get(roomId));

export const getGameText = textId => {
  const text = texts[textId];
  return text;
};

const checkGameEnded = roomId => {
  const room = roomsDatabase.getRoomById(roomId);
  return room?.users.every(user => user.finished !== undefined);
};

const gameOver = (roomId, socket) => {
  clearGameTimer(roomId);
  commentator.gameOver(roomId, socket);
  roomsDatabase.resetRoomStatus(roomId);
  socket.to(roomId).emit('GAME_ENDED');
  socket.emit('GAME_ENDED');
  updateActiveRooms(socket);
};

export const userFinished = (roomId, userId, socket) => {
  roomsDatabase.setUserFinished(roomId, userId);
  if (checkGameEnded(roomId)) {
    gameOver(roomId, socket);
  }
};

const userProgressPercent = (currentCharacter, totalCharacters) => Math.round(
  (currentCharacter / totalCharacters) * 100
);

const userProgress = (currentCharacter, totalCharacters) => ({
  currentCharacter,
  totalCharacters,
  percent: userProgressPercent(currentCharacter, totalCharacters)
});

export const updateRoomProgress = (progress, roomId, socket) => {
  const { id: userId } = socket;
  const { currentCharacter, totalCharacters } = progress;
  const updatedProgress = userProgress(currentCharacter, totalCharacters);
  const room = roomsDatabase.updateGameProgress(socket.id, roomId, updatedProgress);
  socket.to(roomId).emit('UPDATE_ROOM', room);
  socket.emit('UPDATE_ROOM', room);

  commentator.progress(room, userId, socket);
  if (currentCharacter === totalCharacters) {
    userFinished(roomId, userId, socket);
  }
};

const untilEndCountdown = (roomId, socket) => {
  let timeLeft = SECONDS_FOR_GAME;
  socket.to(roomId).emit('TIME_UNTIL_END', timeLeft);
  socket.emit('TIME_UNTIL_END', timeLeft);

  const timeBeforeEnd = setInterval(() => {
    timeLeft -= 1;
    socket.to(roomId).emit('TIME_UNTIL_END', timeLeft);
    socket.emit('TIME_UNTIL_END', timeLeft);

    if (timeLeft === 0) {
      clearGameTimer(roomId);
      gameOver(roomId, socket);
    }
  }, 1000);
  setGameTimer(roomId, timeBeforeEnd);
};

const startCountdown = (roomId, socket) => {
  const randomTextIndex = Math.floor(Math.random() * texts.length);
  const toSend = { textId: randomTextIndex, roomId };
  socket.to(roomId).emit('GAME_COUNTDOWN_START', toSend);
  socket.emit('GAME_COUNTDOWN_START', toSend);

  let timeLeft = SECONDS_TIMER_BEFORE_START_GAME;
  socket.to(roomId).emit('TIME_UNTIL_START', timeLeft);
  socket.emit('TIME_UNTIL_START', timeLeft);

  const timeBeforeStart = setInterval(() => {
    timeLeft -= 1;

    socket.to(roomId).emit('TIME_UNTIL_START', timeLeft);
    socket.emit('TIME_UNTIL_START', timeLeft);
    if (timeLeft === 0) {
      clearGameTimer(roomId);
      untilEndCountdown(roomId, socket);
    }
  }, 1000);
  commentator.createCommentator(roomId, socket);
  setGameTimer(roomId, timeBeforeStart);
};

export const checkGameReadyToStart = room => (
  room?.users.every(user => user.ready)
  && !room?.gameStartedAt
  && room?.users.length >= MINIMUM_USERS_FOR_GAME_START
);

export const userLeftRoom = (roomId, socket) => {
  const room = leaveRoom(roomId, socket);
  if (!room) {
    commentator.deleteCommentator(roomId);
    clearInterval(timers.get(roomId));
  } else {
    if (checkGameReadyToStart(room)) {
      roomsDatabase.setStartGame(roomId);
      startCountdown(roomId, socket);
    }
    if (checkGameEnded(roomId)) {
      gameOver(roomId, socket);
    }
  }
  updateActiveRooms(socket);
};

export const userJoinedRoom = (roomId, socket) => {
  joinRoom(roomId, socket);
  updateActiveRooms(socket);
};

export const userCreatedRoom = (roomId, socket) => {
  const newRoom = createNewRoom(roomId, socket);
  if (newRoom) {
    joinRoom(roomId, socket);
    updateActiveRooms(socket);
  }
};

export const updateRoom = (roomId, socket) => {
  const room = roomsDatabase.toggleUserReady(roomId, socket.id);
  if (room) {
    socket.to(roomId).emit('UPDATE_ROOM', room);
    socket.emit('UPDATE_ROOM', room);
    if (checkGameReadyToStart(room)) {
      roomsDatabase.setStartGame(roomId);
      startCountdown(roomId, socket);
      updateActiveRooms(socket);
    }
  }
};
