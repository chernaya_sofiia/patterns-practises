/* eslint-disable */
import { addClass, removeClass } from '../helpers/helper.mjs';
import { createRoomCard } from '../helpers/cardHelper.mjs';
import { 
  renderRoom,
  clearRoom,
  updateUserlist,
} from '../roomView.mjs';
/* eslint-enable */

const roomsPageElement = document.getElementById('rooms-page');
const gamePageElement = document.getElementById('game-page');
const roomsContainer = document.getElementById('rooms-container');

export const updateRooms = (socket, rooms) => {
  const allRooms = rooms.map(room => createRoomCard(socket, room));
  roomsContainer.innerHTML = '';
  roomsContainer.append(...allRooms);
};

export const createNewRoom = socket => {
  const name = prompt('Enter room name');
  if (!name) {
    return;
  }
  socket.emit('CREATE_NEW_ROOM', name);
};

export const joinRoom = (socket, room) => {
  renderRoom(socket, room);
  removeClass(gamePageElement, 'display-none');
  addClass(roomsPageElement, 'display-none');
};

export const leaveRoom = () => {
  removeClass(roomsPageElement, 'display-none');
  addClass(gamePageElement, 'display-none');
  clearRoom();
};

export const updateRoom = room => {
  updateUserlist(room.users);
};

