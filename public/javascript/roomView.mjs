/* eslint-disable */
import { createElement, addClass, removeClass } from './helpers/helper.mjs';
import {
  hideCountdownStart,
  hideCountdownEnd,
  hideText,
  showButtons,
  showText,
  showCountdownEnd,
  setInnerText,
  createRoomHeader,
  createBackButton,
  createCountdown,
  clearGameScreen
} from './helpers/roomViewHelper.mjs';
/* eslint-enable */

const userCard = user => {
  const currentUser = window.sessionStorage.username === user.username;
  const card = createElement({
    tagName: 'div',
    className: 'no-select'
  });

  const attributes = currentUser ? { id: 'active-user' } : {};
  const className = user.ready ? 'ready-circle active' : 'ready-circle';
  const readyCircle = createElement({
    tagName: 'div',
    className,
    attributes
  });

  const name = createElement({
    tagName: 'span'
  });
  name.innerText = currentUser
    ? `${user.username} (you)`
    : user.username;
  const progressBar = createElement({
    tagName: 'div',
    className: 'progress-bar'
  });
  const progressBarInner = createElement({
    tagName: 'div',
    className: 'progress-bar-inner'
  });
  const progress = user?.progress.percent;
  if (progress >= 100) {
    addClass(progressBarInner, 'finished');
  }
  progressBarInner.style.width = `${progress}%`;
  progressBar.append(progressBarInner);

  [readyCircle, name, progressBar].forEach(it => card.append(it));

  return card;
};

function userReady(socket, roomId) {
  socket.emit('USER_READY', roomId);
}

export const addReadyButton = (socket, roomId) => {
  const gameScreen = document.getElementById('game-screen');
  const button = createElement({
    tagName: 'button',
    className: 'ui-button ready-button',
    attributes: { id: 'ready-button' }
  });
  button.innerText = 'Ready';

  let toggleReady = false;
  const readyCircle = document.getElementById('active-user');
  button.addEventListener('click', () => {
    toggleReady = !toggleReady;
    button.innerText = toggleReady ? 'Not ready' : 'Ready';
    if (toggleReady) {
      addClass(readyCircle, 'active');
    } else {
      removeClass(readyCircle, 'active');
    }
    userReady(socket, roomId);
  });
  gameScreen.append(button);
};

const onLeaveRoom = (socket, id) => {
  socket.emit('LEAVE_ROOM', id);
};

export const renderRoom = (socket, { id, users }) => {
  const roomInfo = document.getElementById('room-info');

  const roomName = createRoomHeader(id);
  roomInfo.append(roomName);

  const leaveRoomBtn = createBackButton();
  roomInfo.append(leaveRoomBtn);
  leaveRoomBtn.addEventListener('click', () => onLeaveRoom(socket, id));

  const userslist = users.map(userCard);

  const usersElement = document.getElementById('users');
  usersElement.append(...userslist);

  addReadyButton(socket, id);

  const gameScreen = document.getElementById('game-screen');
  gameScreen.append(createCountdown('countdown-start'));
  gameScreen.append(createCountdown('countdown-end'));

  const textField = createElement({
    tagName: 'p',
    className: 'game-text display-none',
    attributes: { id: 'game-text' }
  });
  gameScreen.append(textField);
};

export const updateUserlist = users => {
  const usersElement = document.getElementById('users');
  const userslist = users.map(userCard);
  usersElement.innerHTML = '';
  usersElement.append(...userslist);
};

export function clearRoom() {
  setInnerText('room-info', '');

  const usersElement = document.getElementById('users');
  usersElement.innerHTML = '';

  clearGameScreen();
}

export function addText(text) {
  const textElement = document.getElementById('game-text');
  textElement.innerHTML = '';

  text.split('').forEach(character => {
    const charSpan = createElement({ tagName: 'span' });
    charSpan.innerText = character;
    textElement.append(charSpan);
  });
}

export const resetUsersStatus = () => {
  const progressBar = document.querySelectorAll('.progress-bar-inner');
  const userReadyCircles = document.querySelectorAll('.ready-circle');
  if (progressBar) {
    // eslint-disable-next-line
    progressBar.forEach(bar => bar.style.width = '0%')
    userReadyCircles.forEach(circle => removeClass(circle, 'active'));
  }
};

export const gameStartedView = () => {
  hideCountdownStart();
  showText();
  showCountdownEnd();
};

export const gameEndedView = () => {
  showButtons();
  hideText();
  setInnerText('countdown-start', '');
  setInnerText('countdown-end', '');
  hideCountdownEnd();
  resetUsersStatus();
};
