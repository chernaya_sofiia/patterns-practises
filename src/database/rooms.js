import usersDatabase from './users';

const availableRooms = [];

function createRoomUser(user) {
  return {
    ...user,
    progress: { currentCharacter: 0, totalCharacters: 0, percent: 0 },
    ready: false,
    finished: undefined
  };
}

function getRoomById(id) {
  return availableRooms.find(room => room.id === id);
}

function createRoom(roomId) {
  availableRooms.push({
    id: roomId,
    users: [],
    full: false,
    gameStartedAt: undefined
  });
  return getRoomById(roomId);
}

function getActiveRooms() {
  return availableRooms.filter(room => !room.full && !room.gameStartedAt);
}

function addUserToRoom(roomId, userId) {
  const room = getRoomById(roomId);
  if (room) {
    const user = usersDatabase.getUserById(userId);
    room.users.push(createRoomUser(user));
    const updated = getRoomById(roomId);
    return updated;
  }
}

function toggleUserReady(roomId, userId) {
  const room = getRoomById(roomId);
  if (room) {
    const userToUpdate = room.users.find(user => user.id === userId);
    // if user establish new connection while in room
    // he still can click "Ready button"
    if (userToUpdate !== undefined) {
      userToUpdate.ready = !userToUpdate.ready;
      return room;
    }
  }
}

function resetRoomStatus(roomId) {
  const room = getRoomById(roomId);
  if (room) {
    const { users } = room;
    room.gameStartedAt = undefined;
    const updatedUsers = users.map(user => createRoomUser(user));
    room.users = updatedUsers;
  }
}

function removeUserFromRoom(roomId, userId) {
  const room = getRoomById(roomId);
  if (room) {
    const index = room.users.findIndex(user => user.id === userId);
    if (index !== -1) {
      return room.users.splice(index, 1)[0];
    }
  }
}

function deleteRoom(roomId) {
  const index = availableRooms.findIndex(room => room.id === roomId);
  if (index !== -1) {
    return availableRooms.splice(index, 1)[0];
  }
}

function setRoomFull(roomId, full = true) {
  const room = getRoomById(roomId);
  room.full = full;
}

function getRoomFull(roomId) {
  const { full } = getRoomById(roomId);
  return full;
}

function setStartGame(roomId) {
  const room = getRoomById(roomId);
  room.gameStartedAt = Date.now();
}

function updateGameProgress(userId, roomId, progress) {
  const room = getRoomById(roomId);
  if (room?.users.length) {
    const userToUpdate = room?.users.find(user => user.id === userId);
    userToUpdate.progress = progress;
    return room;
  }
}

function setUserFinished(roomId, userId) {
  const room = getRoomById(roomId);
  if (room?.users.length) {
    const userToUpdate = room.users.find(user => user.id === userId);
    userToUpdate.finished = Date.now();
    return room;
  }
}

export default {
  getActiveRooms,
  getRoomById,
  createRoom,
  addUserToRoom,
  removeUserFromRoom,
  deleteRoom,
  setRoomFull,
  getRoomFull,
  toggleUserReady,
  resetRoomStatus,
  setStartGame,
  updateGameProgress,
  setUserFinished
};
