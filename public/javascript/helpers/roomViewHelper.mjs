/* eslint-disable */
import { createElement, addClass, removeClass } from './helper.mjs';
/* eslint-enable */

export const createRoomHeader = roomName => {
  const roomHeader = createElement({
    tagName: 'h2',
    className: 'room-name',
    attributes: { id: 'room-name' }
  });
  roomHeader.innerText = roomName;
  return roomHeader;
};

export const createBackButton = () => {
  const button = createElement({
    tagName: 'button',
    className: 'ui-button',
    attributes: { id: 'leave-room' }
  });
  button.innerText = 'Back to the rooms';
  return button;
};

export const createCountdown = id => {
  const countDown = createElement({
    tagName: 'span',
    className: `${id} display-none`,
    attributes: { id }
  });
  return countDown;
};

export const showElement = elementId => {
  const countDown = document.getElementById(elementId);
  removeClass(countDown, 'display-none');
};

export const hideElement = elementId => {
  const countDown = document.getElementById(elementId);
  addClass(countDown, 'display-none');
};

export const setInnerText = (elementId, text) => {
  const element = document.getElementById(elementId);
  element.innerText = text || '';
};

export const showButtons = () => {
  showElement('leave-room');
  showElement('ready-button');
};

export const hideButtons = () => {
  hideElement('leave-room');
  hideElement('ready-button');
};

export const showText = () => {
  showElement('game-text');
};

export const hideText = () => {
  hideElement('game-text');
};

export const showCountdownEnd = () => {
  showElement('countdown-end');
};

export const hideCountdownEnd = () => {
  hideElement('countdown-end');
};

export const showCountdownStart = () => {
  showElement('countdown-start');
};

export const hideCountdownStart = () => {
  hideElement('countdown-start');
};

export const clearGameScreen = () => {
  const gameScreen = document.getElementById('game-screen');
  gameScreen.innerHTML = '';
};

export const showCommentator = () => {
  showElement('commentator');
};

export const hideCommentator = () => {
  hideElement('commentator');
};
